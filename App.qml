

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick 2.8
import UIControls 1.0
import CloudAccess 1.0
import AppComponents 1.0
import "PayDesk"

Item {
    ProductOverview {
        id: productOverview
        anchors.fill: parent
        labelFontSize: root.labelFontSize
        priceFontSize: root.priceFontSize
        anchors.rightMargin: (parent.width / 3) + 10
        onClicked: {
            stackView.currentItem.addItem(item)
        }
    }

    Rectangle {
        id: stackContainer
        anchors.right: parent.right
        color: Colors.darkBlue
        width: parent.width / 3
        height: parent.height

        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: .15
        }

        states: [
            State {
                name: "expanded"
                when: stackView.currentItem.viewId === "checkout"

                PropertyChanges {
                    target: productOverview
                    opacity: 0
                }

                PropertyChanges {
                    target: stackContainer
                    width: root.width
                }
            }
        ]

        transitions: [
            Transition {
                ParallelAnimation {
                    NumberAnimation {
                        duration: 300
                        property: "opacity"
                    }

                    NumberAnimation {
                        duration: 600
                        property: "width"
                        easing.type: Easing.OutQuint
                    }
                }
            }
        ]

        Rectangle {
            width: root.width / 3
            height: parent.height
            color: Colors.darkBlue
            anchors.horizontalCenter: parent.horizontalCenter

            Stack {
                id: stackView
                clip: true
                anchors.fill: parent
                initialItem: billComponent

                Timer {
                    id: resetTimer
                    running: stackView.depth > 1 || stackView.get(
                                 0).itemModel.count > 0
                    interval: 1000 * 60 * 5
                    onTriggered: {
                        stackView.pop(null)
                        stackView.get(0).clear()
                        productOverview.reset()
                    }
                }

                Rectangle {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    anchors.bottomMargin: 109
                    anchors.bottom: parent.bottom
                    z: 1
                    height: 1
                    color: Colors.white_op30
                }
            }
        }
    }

    Item {
        DeviceModel {
            id: payDot
            resource: root.dotResource
        }

        Component {
            id: billComponent
            Bill {
                id: bill
                property string viewId: "bill"
                deviceModel: payDot
                onCheckoutClicked: {
                    stackView.push(checkoutComponent, {
                                       "billData": billData
                                   }, StackView.ReplaceTransition)
                }
                onShowBalanceClicked: {
                    stackView.push(showBalance, {}, StackView.ReplaceTransition)
                }
            }
        }
        Component {
            id: showBalance
            CheckBalance {
                property string viewId: "checkout"
                onBackClicked: stackView.pop(null, StackView.ReplaceTransition)
                deviceModel: payDot
                onSuccess: stackView.push(displayBalance, {
                                              "cbData": authData
                                          })

                onError: {
                    stackView.push(errorFailed, {
                                       "cbData": data
                                   }, StackView.PushTransition)
                }
            }
        }

        Component {
            id: displayBalance
            ShowBalance {
                property string viewId: "checkout"
                onBackClicked: {
                    stackView.get(0, StackView.DontLoad).clear()
                    stackView.pop(null, StackView.ReplaceTransition)
                }
            }
        }
        Component {
            id: checkoutComponent
            Checkout {
                property string viewId: "checkout"
                id: checkout
                deviceModel: payDot
                onBackClicked: stackView.pop(StackView.ReplaceTransition)
                onSuccess: {
                    productOverview.reset()
                    stackView.push(confirm, {
                                       "cbData": authData
                                   }, StackView.ReplaceTransition)
                }

                onError: {
                    stackView.push(errorFailed, {
                                       "cbData": data
                                   }, StackView.PushTransition)
                }
            }
        }

        Component {
            id: errorFailed
            ErrorFailed {
                property string viewId: "checkout"
                id: checkout
                onBackClicked: stackView.pop()
            }
        }

        Component {
            id: confirm
            FinishSuccess {
                property string viewId: "checkout"
                onBackClicked: {
                    stackView.get(0, StackView.DontLoad).clear()
                    stackView.pop(null, StackView.ReplaceTransition)
                }
            }
        }
    }
}
