

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Item {
    id: docroot

    property bool active: StackView.status === StackView.Active
    signal backClicked
    signal success(var authData)
    signal error(var data)
    property var billData
    property DeviceModel deviceModel
    function checkoutCard(cardID) {
        if (!docroot.active)
            return

        if (internalDot) {
            blizzard.start()
        }

        payService.call("preparebill", {
                            "cartID": docroot.billData.cartID,
                            "bill": JSON.parse(JSON.stringify(
                                                   docroot.billData.bill)),
                            "total": docroot.billData.total,
                            "cardID": cardID
                        }, prepareBillCallback)
    }

    function checkoutUserID(userID) {
        if (!docroot.active)
            return

        payService.call("preparebill", {
                            "cartID": docroot.billData.cartID,
                            "bill": JSON.parse(JSON.stringify(
                                                   docroot.billData.bill)),
                            "total": docroot.billData.total,
                            "userID": userID
                        }, prepareBillCallback)
    }

    function prepareBillCallback(cbData) {
        if (cbData.errcode === 0) {
            payService.call("bill", cbData, docroot.payCallBack)
        } else {
            if (!internalDot) {
                deviceModel.triggerFunction("showError", {})
            }
            docroot.error(cbData)
        }
    }

    function payCallBack(cbData) {
        if (cbData.errcode === 0) {
            if (!internalDot) {
                deviceModel.triggerFunction("showAccept", {})
            }
            docroot.success(cbData)
        } else {
            if (!internalDot) {
                deviceModel.triggerFunction("showError", {})
            }
            docroot.error(cbData)
        }
    }

    ServiceModel {
        id: payService
        service: "payment"
    }

    ColumnLayout {
        width: stackView.width
        height: stackView.height
        spacing: 10

        Column {
            Layout.fillWidth: true
            Layout.minimumHeight: implicitHeight
            //spacer
            Item {
                height: 50
                width: parent.width
            }

            TextLabel {
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTr("Bitte zahlen")
                font.styleName: "Light"
                fontSize: 60
            }

            //            Item {
            //                height: 30
            //                width: parent.width
            //            }
        }
        ColumnLayout {
            width: stackView.width
            anchors.topMargin: 10
            Layout.rightMargin: 10
            Layout.leftMargin: 10
            spacing: 10
            Layout.fillHeight: true
            Layout.fillWidth: true

            Item {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
            PayQRContainer {
                visible: root.enableQRCheckout
                Layout.fillWidth: true
                onCheckoutUserID: docroot.checkoutUserID(userID)
            }
            TextLabel {
                visible: root.enableQRCheckout && deviceModel.deviceOnline
                Layout.alignment: Qt.AlignHCenter
                fontSize: root.labelFontSize
                text: "oder"
            }

            PayCardContainer {
                deviceModel: docroot.deviceModel
                active: docroot.active
                visible: docroot.deviceModel.deviceOnline
                Layout.fillWidth: true
                onCheckoutCard: docroot.checkoutCard(cardID)
            }
            Item {
                Layout.fillHeight: true
                Layout.fillWidth: true
            }
        }
        Item {
            height: 5
            width: parent.width
        }

        // Footer with back button
        Item {
            Layout.minimumHeight: 110
            Layout.maximumHeight: 110
            Layout.fillWidth: true

            BigActionButton {
                id: backButton
                Behavior on opacity {
                    NumberAnimation {}
                }

                width: height
                height: parent.height
                Layout.fillHeight: true
                onClicked: docroot.backClicked()
                opacity: docroot.active ? 1 : 0
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 10

                Icon {
                    iconColor: Colors.white
                    iconSize: 30
                    icon: Icons.leftAngle
                    anchors.centerIn: parent
                }
            }

            Row {
                anchors.left: backButton.right
                anchors.margins: 35
                anchors.verticalCenter: parent.verticalCenter
                spacing: 10

                TextLabel {
                    text: (docroot.billData.total / 100).toLocaleString(
                              Qt.locale("de_DE"))
                    fontSize: Fonts.bigDisplayFontSize
                    anchors.verticalCenter: parent.verticalCenter
                }

                TextLabel {
                    text: "EUR"
                    visible: text !== ""
                    fontSize: Fonts.bigDisplayUnitFontSize
                    color: Colors.lightGrey
                    anchors.verticalCenter: parent.verticalCenter
                }
            }

            LoadingIndicator {
                anchors.right: parent.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.centerIn: undefined
                baseSize: 10
                anchors.rightMargin: 40
            }
        }
    }
}
