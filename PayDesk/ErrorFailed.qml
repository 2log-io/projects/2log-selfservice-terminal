
/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.12

Item {
    id: docroot
    signal backClicked
    property var cbData
    property int timeout: 5000
    property bool active: StackView.status == StackView.Active
    onActiveChanged: if (active) {
                         animation.start()
                         ledAnim.targetColor = "red"
                         ledAnim.start()
                     } else {
                         ledAnim.targetColor = "black"
                         ledAnim.start()
                     }

    ColorAnimation {
        id: ledAnim
        property color targetColor
        alwaysRunToEnd: true
        target: ws2812b
        property: "color"
        to: targetColor
    }

    ColumnLayout {
        width: stackView.width
        height: stackView.height
        anchors.topMargin: 10
        spacing: 0

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true

            TextLabel {
                id: errText
                anchors.right: parent.right
                anchors.left: parent.left
                wrapMode: Text.Wrap
                horizontalAlignment: Qt.AlignHCenter
                text: qsTr("Sorry!")
                font.styleName: "Light"
                anchors.top: parent.top
                anchors.topMargin: 50
                fontSize: 60
            }

            Column {
                id: column
                anchors.centerIn: parent
                spacing: 50

                Icon {
                    id: icon
                    iconColor: Colors.white
                    width: 120
                    height: 120
                    icon: Icons.card
                    iconSize: 100
                    anchors.horizontalCenter: parent.horizontalCenter

                    Rectangle {
                        anchors.left: icon.right
                        anchors.top: icon.bottom
                        anchors.topMargin: -50
                        anchors.leftMargin: -50
                        width: 60
                        height: 60
                        radius: 30
                        color: Colors.warnRed

                        Icon {
                            id: errIcon
                            anchors.centerIn: parent
                            iconColor: Colors.white
                            icon: Icons.question
                            iconSize: 38
                        }
                    }
                }

                TextLabel {
                    id: errDescription
                    wrapMode: Text.Wrap
                    horizontalAlignment: Qt.AlignHCenter
                    width: errText.width
                    fontSize: root.labelFontSize
                }
            }
        }

        Item {
            Layout.minimumHeight: 4
            Layout.maximumHeight: 4
            width: parent.width - 20

            NumberAnimation {
                id: animation
                from: docroot.width - 20
                to: 0
                duration: docroot.timeout
                onRunningChanged: if (!running)
                                      docroot.backClicked()
                target: progress
                property: "width"
            }

            Rectangle {
                x: 10
                id: progress
                height: parent.height
                color: Colors.warnRed
            }
        }

        Item {
            Layout.minimumHeight: 110
            Layout.maximumHeight: 110
            Layout.fillWidth: true

            BigActionButton {
                Behavior on opacity {
                    NumberAnimation {}
                }

                width: height
                height: parent.height
                Layout.fillHeight: true
                onClicked: docroot.backClicked()
                opacity: docroot.active ? 1 : 0
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 10

                Icon {
                    iconColor: Colors.white
                    iconSize: 30
                    icon: Icons.leftAngle
                    anchors.centerIn: parent
                }
            }
        }

        states: [
            State {
                name: "unknown"
                when: cbData.errcode === -2
                PropertyChanges {
                    target: errIcon
                }
                PropertyChanges {
                    target: errDescription
                    text: qsTr("Unbekannter Benutzer")
                }
            },
            State {
                name: "alreadypaid"
                when: cbData.errcode === -4
                PropertyChanges {
                    target: errDescription
                }
                PropertyChanges {
                    target: errText
                    text: qsTr("Der Einkauf wurde bereits bezahlt")
                }
            },
            State {
                name: "creditLimit"
                when: cbData.errcode === -5
                PropertyChanges {
                    target: errIcon
                    icon: Icons.warning2
                }
                PropertyChanges {
                    target: icon
                    icon: Icons.money
                }
                PropertyChanges {
                    target: errDescription
                    text: qsTr("Dein Guthaben reicht leider nicht aus")
                }
            }
        ]
    }
}
