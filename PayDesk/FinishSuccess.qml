
/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.0
import QtQuick.Controls 2.12

Item {
    id: docroot
    signal backClicked
    property var cbData
    property int timeout: 5000
    property string thankYouMessage: "Danke, " + docroot.cbData.username + "!"
    property bool active: StackView.status == StackView.Active
    onActiveChanged: if (active) {
                         animation.start()
                         ledAnim.targetColor = Colors.okGreen
                         ledAnim.start()
                     } else {
                         ledAnim.targetColor = "black"
                         ledAnim.start()
                     }

    ColumnLayout {
        width: stackView.width
        height: stackView.height
        anchors.topMargin: 10
        spacing: 0

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true

            TextLabel {
                id: headline
                horizontalAlignment: Qt.AlignHCenter
                anchors.right: parent.right
                anchors.left: parent.left
                wrapMode: Text.Wrap
                text: docroot.thankYouMessage
                font.styleName: "Light"
                anchors.top: parent.top
                anchors.topMargin: 50
                fontSize: 60
            }

            Column {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.top: headline.bottom
                anchors.topMargin: 60

                width: parent.width
                spacing: 100

                RoundGravatarImage {
                    id: image
                    eMail: docroot.cbData.eMail
                    width: 180
                    anchors.horizontalCenter: parent.horizontalCenter
                    height: 180

                    Rectangle {
                        anchors.left: image.right
                        anchors.top: image.bottom
                        anchors.topMargin: -50
                        anchors.leftMargin: -50
                        width: 60
                        height: 60
                        radius: 30
                        color: Colors.okGreen

                        Icon {
                            anchors.centerIn: parent
                            iconColor: Colors.white
                            icon: Icons.check
                            iconSize: 38
                        }
                    }
                }

                Column {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: 16
                    TextLabel {
                        fontSize: 18
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.styleName: "Light"
                        text: "Dein neuer Kontostand"
                    }
                    Row {
                        anchors.horizontalCenter: parent.horizontalCenter
                        spacing: 20
                        TextLabel {
                            font.styleName: "Light"
                            anchors.verticalCenter: parent.verticalCenter
                            fontSize: 60
                            text: (docroot.cbData.balance / 100).toLocaleString(
                                      Qt.locale("de_DE"))
                        }

                        TextLabel {
                            Layout.alignment: Qt.AlignVCenter
                            text: "EUR"
                            font.styleName: "Light"
                            anchors.verticalCenter: parent.verticalCenter
                            fontSize: 50
                            color: Colors.lightGrey
                        }
                    }
                }
            }
        }

        ColorAnimation {
            id: ledAnim
            property color targetColor
            target: ws2812b
            property: "color"
            alwaysRunToEnd: true
            to: targetColor
        }

        Item {
            Layout.minimumHeight: 4
            Layout.maximumHeight: 4
            Layout.fillWidth: true

            NumberAnimation {
                id: animation
                from: docroot.width - 20
                to: 0
                duration: docroot.timeout
                onRunningChanged: if (!running)
                                      docroot.backClicked()
                target: progress
                property: "width"
            }

            Rectangle {
                x: 10
                id: progress
                height: parent.height
                color: Colors.okayGreen
            }
        }

        Item {
            Layout.minimumHeight: 110
            Layout.maximumHeight: 110
            Layout.fillWidth: true

            BigActionButton {
                Behavior on opacity {
                    NumberAnimation {}
                }

                width: height
                height: parent.height
                Layout.fillHeight: true
                onClicked: docroot.backClicked()
                opacity: docroot.active ? 1 : 0
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 10

                Icon {
                    iconColor: Colors.white
                    iconSize: 30
                    icon: Icons.check
                    anchors.centerIn: parent
                }
            }
        }
    }
}
