

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14
import com.scythestudio.scodes 1.0

Item {
    id: docroot
    height: 160

    signal checkoutUserID(string userID)

    Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .05
        radius: 0
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 30
        spacing: 30

        Item {
            Layout.fillHeight: true
            Layout.minimumWidth: 110
            Layout.maximumWidth: 110

            Image {
                id: image
                anchors.centerIn: parent
                width: 110
                height: 110
                cache: false

                SynchronizedObjectModel {
                    id: authenticatorCodeModel
                    resource: "codeAuthenticator"
                    onEventReceived: {
                        var userID = data.id
                        docroot.checkoutUserID(userID)
                    }
                }

                SBarcodeGenerator {
                    id: barcodeGenerator
                    background: "transparent"
                    foreground: Colors.white

                    property string code: authenticatorCodeModel.initialized ? authenticatorCodeModel.code : ""
                    Component.onCompleted: barcodeGenerator.setFormat("QRCode")
                    onCodeChanged: {
                        barcodeGenerator.generate(code)
                    }

                    onGenerationFinished: {
                        if (error == "") {
                            parent.source = ""
                            parent.source = "file:///" + barcodeGenerator.filePath
                        } else {
                            consoe.log(error)
                        }
                    }
                }
            }
        }

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
            TextLabel {
                opacity: .8
                fontSize: root.priceFontSize
                anchors.right: parent.right
                anchors.left: parent.left
                wrapMode: Text.Wrap
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Scanne jetzt den QR Code mit deiner 2log App.")
            }
        }
    }
}
