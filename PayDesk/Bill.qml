

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Item {
    id: docroot
    //color: Colors.darkBlue
    signal checkoutClicked(var billData)
    signal showBalanceClicked
    property bool active: StackView.status === StackView.Active
    property string price
    property DeviceModel deviceModel
    property alias itemModel: listModel
    onActiveChanged: if (active) {
                         deviceModel.getProperty("state").value = 0
                         ws2812b.color = "black"
                     }

    ListModel {
        id: listModel
        onCountChanged: {
            var price = 0
            for (var i = 0; i < listModel.count; i++) {
                price += listModel.get(i).price
            }
            docroot.price = price
        }
    }

    function addItem(item) {
        listModel.append(item)
    }

    function clear() {
        listModel.clear()
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.topMargin: 0
        spacing: 0

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true

            ListView {
                id: listView
                clip: true
                model: listModel
                anchors.fill: parent
                anchors.topMargin: 10
                anchors.rightMargin: 10
                anchors.leftMargin: 10

                delegate: SwipeDelegate {
                    clip: true
                    id: swipeDelegate
                    width: parent !== null ? parent.width : 0
                    height: 60
                    onClicked: if (swipe.complete)
                                   swipe.close()
                               else
                                   swipe.open(SwipeDelegate.Right)

                    background: Item {

                        Rectangle {
                            color: Colors.darkBlue
                            anchors.fill: parent
                            visible: swipeDelegate.swipe.position !== 0
                        }
                        width: parent.width

                        RowLayout {
                            anchors.margins: 20
                            anchors.fill: parent

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                TextLabel {
                                    anchors.verticalCenter: parent.verticalCenter
                                    width: 200
                                    text: name
                                    fontSize: root.labelFontSize
                                }
                            }

                            Item {
                                Layout.fillWidth: true
                                Layout.fillHeight: true
                                Row {
                                    anchors.right: parent.right
                                    anchors.verticalCenter: parent.verticalCenter
                                    TextLabel {
                                        fontSize: root.labelFontSize
                                        horizontalAlignment: Qt.AlignRight
                                        anchors.verticalCenter: parent.verticalCenter
                                        text: (price / 100).toLocaleString(
                                                  Qt.locale("de_DE")) + " "
                                    }

                                    TextLabel {
                                        anchors.verticalCenter: parent.verticalCenter
                                        text: " EUR"
                                        fontSize: root.priceFontSize
                                        color: Colors.lightGrey
                                    }
                                }
                            }
                        }
                    }
                    Rectangle {
                        anchors.right: parent.right
                        anchors.left: parent.left
                        height: 1
                        anchors.bottom: parent.bottom
                        color: Colors.white_op5
                        visible: index != listView.count - 1
                    }

                    swipe.right: Icon {
                        visible: swipeDelegate.swipe.position !== 0
                        id: deleteLabel
                        icon: Icons.trash
                        iconColor: Colors.white
                        height: parent.height
                        width: height
                        anchors.right: parent.right
                        MouseArea {
                            anchors.fill: parent
                            onClicked: listView.model.remove(index)
                        }
                        color: deleteLabel.SwipeDelegate.pressed ? Qt.darker(
                                                                       "tomato",
                                                                       1.1) : "tomato"
                    }
                }
            }
        }
        BillCheckoutButtons {
            price: docroot.price
            onCancel: listModel.clear()
            onShowBalance: docroot.showBalanceClicked()
            onPrepareCheckout: docroot.prepareCheckout()
            itemCount: listModel.count > 0
        }
    }

    function prepareCheckout() {
        var list = []
        for (var i = 0; i < listModel.count; i++) {
            list.push(listModel.get(i))
        }
        docroot.checkoutClicked({
                                    "total": docroot.price,
                                    "bill": list,
                                    "cartID": cppHelper.createUUID()
                                })
    }
}
