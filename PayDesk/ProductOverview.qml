

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Rectangle {
    id: docroot
    color: Colors.darkBlue
    property int labelFontSize: Fonts.contentFontSize
    property int priceFontSize: Fonts.verySmallControlFontSize

    signal clicked(var item)
    function reset() {
        swipeView.currentIndex = 0
    }
    property var categories: new Set()
    property var catModel: []
    Item {
        anchors.fill: parent

        SynchronizedListModel {
            id: productModel
            resource: "labcontrol_payment/products"
        }

        RoleFilter {
            id: selfServiceFilter
            booleanFilterRoleName: "selfService"
            sourceModel: productModel
            onCountChanged: {
                docroot.categories = new Set()
                for (var i = 0; i < selfServiceFilter.count; i++) {
                    var newIdx = getSourceIndex(i)
                    var item = productModel.get(newIdx)
                    docroot.categories.add(item.category)
                }

                var model = []
                for (var x of docroot.categories.values()) {
                    model.push(x)
                }
                docroot.catModel = model
            }
        }

        ColumnLayout {
            anchors.fill: parent

            SwipeView {
                id: swipeView
                Layout.fillHeight: true
                Layout.fillWidth: true
                clip: true

                Start {
                    welcomeMessage1: root.welcomeMessage1
                    welcomeMessage2: root.welcomeMessage2
                    userInstruction: root.userInstruction
                }

                Repeater {

                    model: {
                        var model = docroot.catModel
                        // insert an empty string at the beginning.
                        // This is the placeholder for the start screen
                        if (model !== undefined)
                            model.unshift("")
                        return model
                    }

                    Item {

                        DynamicGridView {
                            id: layout
                            clip: true
                            anchors.fill: parent
                            anchors.margins: 10

                            RoleFilter {
                                id: roleFilter
                                sourceModel: selfServiceFilter
                                stringFilterSearchRole: "category"
                                sortRoleString: "name"
                                searchString: modelData
                                inverse: true
                            }

                            cellHeight: 120
                            maxCellWidth: root.maxCellWidth
                            model: roleFilter
                            delegate: Item {
                                width: layout.cellWidth
                                height: layout.cellHeight

                                Item {
                                    anchors.fill: parent
                                    anchors.margins: 2

                                    BigActionButton {
                                        anchors.fill: parent
                                        onClicked: docroot.clicked(
                                                       productModel.get(selfServiceFilter.getSourceIndex(
                                                           roleFilter.getSourceIndex(
                                                               index))))
                                    }

                                    Column {
                                        anchors.centerIn: parent
                                        width: parent.width - 10

                                        spacing: 8
                                        TextLabel {
                                            fontSize: docroot.labelFontSize
                                            width: parent.width
                                            horizontalAlignment: Text.AlignHCenter
                                            wrapMode: Text.Wrap
                                            text: name
                                        }

                                        TextLabel {
                                            fontSize: docroot.priceFontSize
                                            color: Colors.white_op50
                                            anchors.horizontalCenter: parent.horizontalCenter
                                            text: (price / 100).toLocaleString(
                                                      Qt.locale(
                                                          "de_DE")) + " EUR"
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Item {
                id: bottomRow
                Layout.fillWidth: true
                Layout.minimumHeight: 110
                Layout.maximumHeight: 110

                Rectangle {
                    anchors.right: parent.right
                    anchors.left: parent.left
                    anchors.margins: 10
                    height: 1
                    color: Colors.white_op30
                }

                Item {
                    width: row.width / (repeater.count + 2)
                    height: parent.height - 10

                    x: 10 + ((swipeView.contentItem.contentX
                              - swipeView.contentItem.originX) / swipeView.width) * width

                    Rectangle {
                        id: background
                        anchors.topMargin: 10
                        anchors.fill: parent
                        color: Colors.white_op5
                    }

                    Rectangle {
                        anchors.top: parent.top
                        color: Colors.highlightBlue
                        height: 1
                        width: parent.width
                    }
                }

                Row {
                    id: row
                    anchors.fill: parent
                    anchors.rightMargin: 10
                    anchors.leftMargin: 10
                    anchors.topMargin: 10
                    anchors.bottomMargin: 10

                    AbstractButton {
                        width: row.width / (repeater.count + 2)
                        height: parent.height
                        onClicked: swipeView.currentIndex = 0
                        checked: swipeView.currentIndex == 0

                        TextLabel {
                            anchors.centerIn: parent
                            horizontalAlignment: Text.AlignHCenter
                            width: parent.width - 20
                            text: "Start"
                            fontSize: root.labelFontSize
                        }
                    }

                    AbstractButton {
                        width: row.width / (repeater.count + 2)
                        height: parent.height
                        onClicked: swipeView.currentIndex = 1
                        checked: swipeView.currentIndex == 1

                        TextLabel {
                            anchors.centerIn: parent
                            horizontalAlignment: Text.AlignHCenter
                            width: parent.width - 20
                            text: "Alles"
                            fontSize: root.labelFontSize
                        }
                    }

                    Repeater {
                        id: repeater
                        model: {
                            return docroot.catModel
                        } //productModel.metadata.categories
                        AbstractButton {
                            width: row.width / (repeater.count + 2)
                            height: parent.height
                            onClicked: swipeView.currentIndex = index + 2
                            checked: swipeView.currentIndex == index + 2

                            TextLabel {
                                anchors.centerIn: parent
                                horizontalAlignment: Text.AlignHCenter
                                width: parent.width - 20
                                text: modelData
                                fontSize: root.labelFontSize
                            }
                        }
                    }
                }
            }
        }
    }
}
