
/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import QtQuick 2.0
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0

Item {
    id: docroot
    property string welcomeMessage1
    property string welcomeMessage2
    property string userInstruction

    TextLabel {
        id: clock
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.margins: 20
        fontSize: root.labelFontSize
    }

    Image {
        source: "qrc:/qml/UIControls/Assets/Pics/Logo-2logio.svg"
        height: clock.height
        anchors.top: parent.top
        anchors.right: parent.right
        anchors.margins: 20
        fillMode: Image.PreserveAspectFit
    }

    Timer {
        interval: 1000
        onTriggered: clock.text = Qt.formatTime(new Date(), "hh:mm")
        running: true
        repeat: true
    }

    Column {
        anchors.centerIn: parent
        TextLabel {

            text: docroot.welcomeMessage1
            fontSize: 80
        }
        TextLabel {
            id: primaryText
            font.styleName: "Light"
            text: docroot.welcomeMessage2
            opacity: .6
            fontSize: 70
        }

        Item {
            height: 20
            width: 1
        }
        TextLabel {
            width: primaryText.width
            wrapMode: Text.Wrap
            text: docroot.userInstruction
            opacity: .6
            fontSize: root.priceFontSize
        }
    }
}
