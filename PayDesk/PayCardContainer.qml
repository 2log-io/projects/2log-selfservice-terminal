

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Item {
    id: docroot
    height: 160
    Rectangle {
        anchors.fill: parent
        color: "white"
        opacity: .05
        radius: 0
    }

    property bool active
    property DeviceModel deviceModel
    signal checkoutCard(string cardID)

    Connections {
        target: pn532
        enabled: internalDot
        function onCardRead(uid) {
            checkoutCard(uid)
        }
    }

    ColorAnimation {
        id: ledAnim
        property color targetColor
        target: ws2812b
        property: "color"
        to: targetColor
        alwaysRunToEnd: true
        onRunningChanged: if (!running)
                              fadeAnimation.running = docroot.active
    }

    NumberAnimation {
        id: blizzard
        from: 1
        property: "brightness"
        target: ws2812b
        to: 0.05
        duration: 200
        easing.type: Easing.OutCubic
        onRunningChanged: if (running) {
                              fadeAnimation.running = false
                          } else {
                              fadeAnimation.running = docroot.active
                          }
    }

    onActiveChanged: if (internalDot) {
                         if (active) {
                             ws2812b.brightness = 0
                             ws2812b.color = "white"
                             fadeAnimation.start()
                         } else {
                             fadeAnimation.stop()
                             ledAnim.targetColor = "black"
                             ledAnim.start()
                         }
                     } else {
                         deviceModel.getProperty("state").value = active ? 1 : 0
                     }

    Connections {
        target: deviceModel
        function onDataReceived(subject) {
            docroot.checkoutCard(subject)
        }
    }

    ParallelAnimation {
        id: fadeAnimation
        loops: Animation.Infinite
        SequentialAnimation {
            NumberAnimation {
                target: ws2812b
                property: "brightness"
                to: 0.2
                duration: 600
            }

            NumberAnimation {
                target: ws2812b
                to: 0.05
                property: "brightness"
                duration: 600
            }
        }
        //        SequentialAnimation {
        //            NumberAnimation {
        //                target: arrowIcon.anchors
        //                property: "verticalCenterOffset"
        //                to: 10
        //                duration: 600
        //            }

        //            NumberAnimation {
        //                target: arrowIcon.anchors
        //                property: "verticalCenterOffset"
        //                to: -10
        //                duration: 600
        //            }
        //        }
    }

    RowLayout {
        anchors.fill: parent
        anchors.margins: 30
        spacing: 30
        Item {
            Layout.fillHeight: true
            Layout.minimumWidth: 110
            Layout.maximumWidth: 110
            Icon {
                iconColor: Colors.highlightBlue
                anchors.centerIn: parent
                //     height: 100
                icon: Icons.card
                iconSize: 100
            }
        }
        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true
            TextLabel {
                opacity: .8
                fontSize: root.priceFontSize
                anchors.right: parent.right
                anchors.left: parent.left
                wrapMode: Text.Wrap
                anchors.verticalCenter: parent.verticalCenter
                text: qsTr("Halte jetzt deine 2log Karte vor den leuchtenden Kreis an den Kartenleser.")
            }
        }
    }
}
