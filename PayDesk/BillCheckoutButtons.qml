


/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Item {
    id: docroot
    clip: true
    signal prepareCheckout
    signal cancel
    signal showBalance
    property int price
    property int itemCount
    onItemCountChanged: console.log()
    Layout.minimumHeight: 110
    Layout.maximumHeight: 110
    Layout.fillWidth: true

    Item {
        id: checkoutButtons
        opacity: 0
        y: 110
        width: parent.width
        height: parent.height
        RowLayout {
            anchors.fill: parent
            anchors.bottomMargin: 10
            anchors.rightMargin: 10
            anchors.topMargin: 10
            anchors.leftMargin: 10

            BigActionButton {
                Layout.minimumWidth: height
                Layout.maximumWidth: height
                Layout.fillHeight: true
                onClicked: docroot.cancel()
                Icon {
                    iconColor: Colors.warnRed
                    iconSize: 30
                    icon: Icons.cancel
                    anchors.centerIn: parent
                }
            }

            BigPriceActionButton {
                text: (docroot.price / 100).toLocaleString(Qt.locale("de_DE"))
                Layout.fillHeight: true
                buttonIcon: Icons.check
                Layout.fillWidth: true
                onClicked: docroot.prepareCheckout()
            }
        }
    }

    Item {
        id: balanceButton
        opacity: 1
        y: 0
        width: parent.width
        height: parent.height
        BigPriceActionButton {
            anchors.fill: parent
            secondaryText: ""
            anchors.margins: 10
            onClicked: docroot.showBalance()
            text: "Kontostand abfragen"
            fontSize: root.labelFontSize
            buttonIcon: Icons.card
        }
    }

    transitions: [
        Transition {
            from: "withItems"

            SequentialAnimation {
                PropertyAnimation {
                    target: checkoutButtons
                    properties: "opacity, y"
                }
                PropertyAnimation {
                    target: balanceButton
                    properties: "opacity, y"
                }
            }
        },
        Transition {
            to: "withItems"

            SequentialAnimation {
                PropertyAnimation {
                    target: balanceButton
                    properties: "opacity, y"
                }
                PropertyAnimation {
                    target: checkoutButtons
                    properties: "opacity, y"
                }
            }
        }
    ]

    onStateChanged: console.log(state)
    states: [
        State {
            name: "withItems"
            when: docroot.itemCount > 0
            PropertyChanges {
                target: checkoutButtons
                opacity: 1
                y: 0
            }

            PropertyChanges {
                target: balanceButton
                opacity: 0
                y: 110
            }
        }
    ]
}
