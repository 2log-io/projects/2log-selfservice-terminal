

/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick 2.15
import CloudAccess 1.0
import QtQuick.Controls 2.12
import UIControls 1.0
import AppComponents 1.0
import QtQuick.Layouts 1.14

Item {
    id: docroot
    property DeviceModel deviceModel
    property bool active: StackView.status === StackView.Active
    signal backClicked
    signal success(var authData)
    signal error(var data)
    property var billData

    onActiveChanged: if (internalDot) {
                         if (active) {
                             ws2812b.brightness = 0
                             ws2812b.color = "white"
                             fadeAnimation.start()
                         } else {
                             fadeAnimation.stop()
                             ledAnim.targetColor = "black"
                             ledAnim.start()
                         }
                     } else {
                         deviceModel.getProperty("state").value = active ? 1 : 0
                     }

    Connections {
        target: deviceModel
        function onDataReceived(subject) {
            checkout(subject)
        }
    }

    ServiceModel {
        id: payService
        service: "lab"
    }

    function checkout(cardID) {
        if (!docroot.active)
            return

        if (internalDot) {
            blizzard.start()
        }

        payService.call("getUserForCard", {
                            "cardID": cardID
                        }, getUserCallback)
    }

    NumberAnimation {
        id: blizzard
        from: 1
        property: "brightness"
        target: ws2812b
        to: 0.05
        duration: 200
        easing.type: Easing.OutCubic
        onRunningChanged: if (running) {
                              fadeAnimation.running = false
                          } else {
                              fadeAnimation.running = docroot.active
                          }
    }

    Connections {
        target: pn532
        enabled: internalDot
        function onCardRead(uid) {
            console.log(uid)
            checkout(uid)
        }
    }

    function getUserCallback(cbData) {
        if (cbData.errorCode === 0) {
            if (!internalDot) {
                deviceModel.triggerFunction("showAccept", {})
            }
            docroot.success(cbData)
        } else {
            if (!internalDot) {
                deviceModel.triggerFunction("showError", {})
            }
            docroot.error(cbData)
        }
    }

    ColorAnimation {
        id: ledAnim
        property color targetColor
        target: ws2812b
        property: "color"
        to: targetColor
        onRunningChanged: if (!running)
                              fadeAnimation.running = docroot.active
    }

    ParallelAnimation {
        id: fadeAnimation
        loops: Animation.Infinite
        SequentialAnimation {
            NumberAnimation {
                target: ws2812b
                property: "brightness"
                to: 0.2
                duration: 600
            }

            NumberAnimation {
                target: ws2812b
                to: 0.05
                property: "brightness"
                duration: 600
            }
        }
        SequentialAnimation {
            NumberAnimation {
                target: arrowIcon.anchors
                property: "verticalCenterOffset"
                to: 10
                duration: 600
            }

            NumberAnimation {
                target: arrowIcon.anchors
                property: "verticalCenterOffset"
                to: -10
                duration: 600
            }
        }
    }

    ColumnLayout {
        width: stackView.width
        height: stackView.height
        anchors.topMargin: 10
        spacing: 0

        Item {
            Layout.fillHeight: true
            Layout.fillWidth: true

            TextLabel {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.margins: 10
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.Wrap
                text: qsTr("Kontostand abfragen")
                font.styleName: "Light"
                anchors.top: parent.top
                anchors.topMargin: 50
                fontSize: 60
            }

            Column {
                id: column
                anchors.centerIn: parent
                spacing: 50

                Item {
                    height: 30
                    width: 1
                }

                Icon {
                    iconColor: Colors.white
                    width: parent.width
                    height: 100
                    icon: Icons.card
                    iconSize: 100
                }
            }

            Item {
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.bottom: parent.bottom
                anchors.top: column.bottom
                Icon {
                    id: arrowIcon
                    anchors.centerIn: parent
                    anchors.bottomMargin: 50
                    icon: Icons.downArrow
                    iconSize: 80
                }
            }
        }

        Item {
            Layout.minimumHeight: 110
            Layout.maximumHeight: 110
            Layout.fillWidth: true

            BigActionButton {
                Behavior on opacity {
                    NumberAnimation {}
                }

                width: height
                height: parent.height
                Layout.fillHeight: true
                onClicked: docroot.backClicked()
                opacity: docroot.active ? 1 : 0
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.margins: 10

                Icon {
                    iconColor: Colors.white
                    iconSize: 30
                    icon: Icons.leftAngle
                    anchors.centerIn: parent
                }
            }
        }
    }
}
