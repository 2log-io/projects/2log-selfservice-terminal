
/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4
import QtQuick 2.8
import UIControls 1.0
import CloudAccess 1.0
import AppComponents 1.0
import QtQuick.Window 2.12

Window {
    id: root
    width: 1280
    height: 800
    visible: true
    title: qsTr("Hello World")

    property string dotResource
    property string welcomeMessage1: "Hi!"
    property string welcomeMessage2: "Was darf's sein?"
    property string userInstruction: "Wähle ein Produkt und Bezahle mit deiner 2log-Karte.\nWende dich an den Vorstand, falls du noch keine Karte hast."
    property bool enableQRCheckout: false
    property int labelFontSize: 22
    property int priceFontSize: 17
    property int maxCellWidth: 200

    property int appState: Qt.application.state
    property bool isMobile: true
    property bool suspended
    property bool reconnect: false
    property bool loggedOut: true

    Connections {
        target: Connection
        function onStateChanged() {
            if (Connection.state == Connection.STATE_Authenticated) {
                loggedOut = false
            }
        }
    }

    function suspend() {
        if (Connection.state == Connection.STATE_Authenticated) {
            root.suspended = true
            Connection.disconnectServer()
            root.reconnect = true
            root.loggedOut = true
        }
    }

    function activate() {
        if (root.reconnect) {
            root.reconnect = false
            Connection.reconnectServer()
            mobileHelper.hideBottomMenu()
        }
    }

    onAppStateChanged: {
        if (isMobile) {
            switch (appState) {
            case Qt.ApplicationSuspended:
                suspend()
                break
            case Qt.ApplicationActive:
                activate()
                break
            }
        }
    }

    Rectangle {
        anchors.fill: parent
        color: Colors.darkBlue
        Rectangle {
            anchors.fill: parent
            color: "black"
            opacity: .15
        }

        DeviceConnectContainer {
            anchors.centerIn: parent
            visible: Connection.state < Connection.STATE_Connected
        }
    }

    Component.onCompleted: {
        if (serverURL !== "") {
            console.log("foo" + serverURL)
            Connection.autoConnect = false
            Connection.connectToServer(serverURL)
        }
        console.log("DEVICEUUID: "+ deviceUuid)
        StandaloneDevice.type = "Selfservice-Terminal"
        StandaloneDevice.uuid = deviceUuid
        StandaloneDevice.setPermission("lab.admin", true)
        StandaloneDevice.setPermission("lab.canEditDevices", true)
        StandaloneDevice.registerProperty("dotResource", function (data) {
            root.dotResource = data.val
            StandaloneDevice.setProperty("dotResource", root.dotResource)
        })

        StandaloneDevice.registerPropertyWithInitValue("welcomeMessage1",
                                                       function (data) {
                                                           root.welcomeMessage1 = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "welcomeMessage1",
                                                                       root.welcomeMessage1)
                                                       }, root.welcomeMessage1)

        StandaloneDevice.registerPropertyWithInitValue("welcomeMessage2",
                                                       function (data) {
                                                           root.welcomeMessage2 = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "welcomeMessage2",
                                                                       root.welcomeMessage2)
                                                       }, root.welcomeMessage2)

        StandaloneDevice.registerPropertyWithInitValue("userInstruction",
                                                       function (data) {
                                                           root.userInstruction = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "userInstruction",
                                                                       root.userInstruction)
                                                       }, root.userInstruction)

        StandaloneDevice.registerPropertyWithInitValue("labelFontSize",
                                                       function (data) {
                                                           root.labelFontSize = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "labelFontSize",
                                                                       root.labelFontSize)
                                                       }, root.labelFontSize)

        StandaloneDevice.registerPropertyWithInitValue("priceFontSize",
                                                       function (data) {
                                                           root.priceFontSize = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "priceFontSize",
                                                                       root.priceFontSize)
                                                       }, root.priceFontSize)
        StandaloneDevice.registerPropertyWithInitValue("maxCellWidth",
                                                       function (data) {
                                                           root.maxCellWidth = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "maxCellWidth",
                                                                       root.maxCellWidth)
                                                       }, root.maxCellWidth)
        StandaloneDevice.registerPropertyWithInitValue("enableQRCheckout",
                                                       function (data) {
                                                           root.enableQRCheckout = data.val
                                                           StandaloneDevice.setProperty(
                                                                       "enableQRCheckout",
                                                                       root.enableQRCheckout)
                                                       }, root.enableQRCheckout)
        StandaloneDevice.start()
        if (internalDot)
            ws2812b.color = ""
    }

    Timer {
        running: Connection.state <= Connection.STATE_Disconnected
        interval: 5000
        onTriggered: Connection.reconnectServer()
    }

    App {
        visible: Connection.state == Connection.STATE_Authenticated
        anchors.fill: parent
    }

    Column {
        anchors.centerIn: parent
        spacing: 20
        TextLabel {

            fontSize: 30
            visible: Connection.state == Connection.STATE_Authenticating
            text: qsTr("Super, ich bin verbunden!")
        }
        TextLabel {
            opacity: .5
            fontSize: 22
            font.styleName: "Light"
            visible: Connection.state == Connection.STATE_Authenticating
            text: qsTr("Log dich nun in das Admin-Portal ein und registriere\nmich im Device-Explorer als neues Gerät")
        }
    }
}
