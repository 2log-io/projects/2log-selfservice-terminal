/*   2log.io
 *   Copyright (C) 2021 - 2log.io | mail@2log.io,  mail@friedemann-metzger.de
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU Affero General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "InitQuickHub.h"
#include "Init2logQMLComponents.h"
#include "Init2logQMLControls.h"
#include "CppHelper.h"
#include <QProcessEnvironment>
#include <QThreadPool>

#ifdef RASPBERRY
#include "qpn532.h"
#include "qws2812b.h"
#endif

#ifdef ANDROID
#include "SleepAvoider.h"
#include <QtAndroid>
#include "AndroidHelper.h"
#endif

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    qputenv("QT_QPA_EGLFS_HIDECURSOR","1");
    qputenv("QT_SCALE_FACTOR","1");
#endif

    QCoreApplication::setOrganizationName("2log");
    QCoreApplication::setOrganizationDomain("2log.io");
    QCoreApplication::setApplicationName("2log SelfService");

    QThreadPool::globalInstance()->setMaxThreadCount(5);
    QGuiApplication app(argc, argv);
    QQmlApplicationEngine engine;

#ifdef RASPBERRY
    QPN532* pn532 = new QPN532();
    engine.rootContext()->setContextProperty("pn532",pn532);
    QWS2812B* ws2812b = new QWS2812B();
    engine.rootContext()->setContextProperty("ws2812b", ws2812b);
#endif
    InitQuickHub::registerTypes("CloudAccess");
    Init2logQMLComponents::registerTypes("AppComponents", &engine);
    Init2logQMLControls::registerTypes("UIControls", &engine);
    CppHelper* keyHandler = new CppHelper(qApp);
    qApp->installEventFilter(keyHandler);
    engine.rootContext()->setContextProperty("cppHelper",keyHandler);
#ifdef RASPBERRY
    engine.rootContext()->setContextProperty("internalDot", true);
#else
    engine.rootContext()->setContextProperty("internalDot", false);
#endif
    QString _2logServer = QProcessEnvironment::systemEnvironment().value("SERVER_2LOG", "");
    QString _deviceUuid = QProcessEnvironment::systemEnvironment().value("DEVICE_UUID", "selfservice01");
    engine.rootContext()->setContextProperty("serverURL", _2logServer);
    engine.rootContext()->setContextProperty("deviceUuid", _deviceUuid);

#ifdef ANDROID
    SleepAvoider* avoider = new SleepAvoider(qApp);
    AndroidHelper* helper = new AndroidHelper(qApp);
    helper->hideBottomMenu();
    engine.rootContext()->setContextProperty("mobileHelper", helper);
    avoider->dolock();

#endif

    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
