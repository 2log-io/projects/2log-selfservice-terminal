#include "AndroidHelper.h"
#include <QDebug>

AndroidHelper::AndroidHelper(QObject *parent)
    : QObject{parent}
{

}


QString AndroidHelper::getIntentString()
{
    QAndroidJniObject activity = QtAndroid::androidActivity();
    if (activity.isValid()) {
        qDebug() << "activity";
        QAndroidJniObject intent = activity.callObjectMethod("getIntent", "()Landroid/content/Intent;");
        if (intent.isValid()) {
            qDebug()<< intent.callObjectMethod("getDataString", "()Ljava/lang/String;").toString();
            return intent.callObjectMethod("getData", "()Landroid/net/Uri;").toString();
        }}
    return "";
}


void AndroidHelper::setMenuStyle()
{
    QtAndroid::runOnAndroidThread([=]()
    {
        QAndroidJniObject window = QtAndroid::androidActivity().callObjectMethod("getWindow", "()Landroid/view/Window;");
        window.callMethod<void>("addFlags", "(I)V", 0x80000000);
        window.callMethod<void>("clearFlags", "(I)V", 0x04000000);
        window.callMethod<void>("setStatusBarColor", "(I)V", 0xff202428); // Desired statusbar color
        window.callMethod<void>("setNavigationBarColor", "(I)V", 0xff202428); // Desired statusbar color
                                  });
}

void AndroidHelper::hideBottomMenu()
{
    QtAndroid::runOnAndroidThread([=]()
    {
      QAndroidJniObject window = QtAndroid::androidActivity().callObjectMethod("getWindow", "()Landroid/view/Window;");
      QAndroidJniObject decorView = window.callObjectMethod("getDecorView", "()Landroid/view/View;");
      int flags = 0x00000002 | 0x00000400 | 0x00000100 | 0x00000200 | 0x00000004 | 0x00001000;
      decorView.callMethod<void>("setSystemUiVisibility", "(I)V", flags);
    });
}



